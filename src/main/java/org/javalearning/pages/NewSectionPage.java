package org.javalearning.pages;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.javalearning.app.JavaLearningApp;

import java.awt.*;


/**
 * Страница для создания нового раздела
 */
public class NewSectionPage {
    public static Stage getPage() {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25));
        Scene primaryScene = new Scene(gridPane);
        Label enterName = new Label("Введите название нового раздела:");
        TextField sectionName = new TextField();
        Button addButton = new Button("Добавить");
        addButton.setOnAction(event -> {
            String name =sectionName.getText();
            JavaLearningApp.createButton(name);
        });
        gridPane.add(enterName, 0, 0);
        gridPane.add(sectionName, 0, 1);
        gridPane.add(addButton,0,2);
        Stage sectionPage = new Stage();
        sectionPage.setTitle("Новый раздел");
        sectionPage.setWidth(screenSize.getWidth() / 2);
        sectionPage.setHeight(screenSize.getHeight() / 2);
        sectionPage.setScene(primaryScene);
        return sectionPage;
    }
}
