package org.javalearning.app;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.javalearning.pages.NewSectionPage;

import java.awt.*;

/**
 * Точка входа в приложение JavaLearning
 */
public class JavaLearningApp extends Application {
    static GridPane pane;

    @Override
    public void start(Stage primaryStage){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        primaryStage.setTitle("Java learning App");
        primaryStage.setWidth(screenSize.getWidth());
        primaryStage.setHeight(screenSize.getHeight());
        pane = new GridPane();
        Button createSection = new Button("Добавить раздел");
        pane.getRowConstraints().add(new RowConstraints(80));
        createSection.setOnAction(event -> NewSectionPage.getPage().show());
        pane.add(createSection, 0, 0, 5, 1);
        createSection.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);
        createSection.setFont(new Font(25));
        Scene primaryScene = new Scene(pane);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    private static boolean isNodeExist(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return true;
            }
        }
        return false;
    }

    public static void createButton(String buttonName) {
        for (int i = 0; i < 5; i++) {
            for (int j = 1; j < 6; j++) {
                if (!isNodeExist(pane, i, j)) {
                    Button newButton = new Button(buttonName);
                    pane.add(newButton, i, j);
                    pane.getColumnConstraints().add(new ColumnConstraints(80));
                    pane.getRowConstraints().add(new RowConstraints(40));
                    newButton.setMaxWidth(Double.MAX_VALUE);
                    newButton.setMaxHeight(Double.MAX_VALUE);
                    return;
                }
            }
        }
    }
}
